#!/bin/bash
# Released under GPLv3+ License
# Danial Behazdi <dani.behzi@ubuntu.com>, 2019-2021

export SHELL=/bin/bash
source .env/bin/activate
python tootification.py
