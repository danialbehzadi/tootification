#! /usr/bin/env python3
# 2019 - Danial Behzadi
# Released under AGPLv3

from mastodon import Mastodon
import config

Mastodon.create_app(
        config.apps_name,
        api_base_url = config.most_inst,
        to_file = config.client_creds
        )

mastodon = Mastodon(
        client_id = config.client_creds,
        api_base_url = config.most_inst
        )

mastodon.log_in(
        config.user_name,
        config.user_pass,
        to_file = config.access_token
        )
