Install
=======
To make it work, do the following:

    $ git clone https://framagit.org/danialbehzadi/tootification.git
    $ cd tootification
    $ python3 -m venv .env
    $ source .env/bin/activate
    (.env) $ pip install -r requirements.txt

Setup
======
First set your own configs in `config.py` file. Then run setup:

    (.env) $ ./setup.py

In case you don't have the password, instead on running setup.py, make a `usercr.secret` file an fill it with access token given to you.

Run
===
To run the program do as following:

    $ ./run.sh

For exit, press `<ctrl>+c` keybinding.
