#!/usr/bin/python3
# Released under GPLv3+ License
# Danial Behazdi <dani.behzi@ubuntu.com>, 2019-2021

apps_name = 'tootification'
most_inst = 'https://persadon.com'
show_time = 20
intr_time = 20

# Remove these lines after setup
user_name = 'email@address.com'
user_pass = 'YourUsersPassword'

# Don't change these lines
client_creds = 'client.secret'
access_token = 'usercr.secret'
